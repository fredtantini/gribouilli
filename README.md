* Fonctionnalités

- se met en plein écran
- joue un son aléatoire à chaque appui de touche (dans le dossier =sounds=)
- affiche une forme ou une image à chaque appui de touche (dans le dossier =images=)
- si un nom de fichier image correspond plus ou moins au nom de fichier son joué, c’est cette image qui est affichée (par exemple si c’est =dog_2.wav= qui est jouée, alors =dog_4.png= peut être affichée (mais pas =cat_2.svg=))
- une combinaison de touche pour quitter (à modifier dans le code si elle ne vous plait pas :þ)
- une combinaison de touche pour tout supprimer
- la souris trace des traits
- les boutons affichent les formes/images

* Défauts
- la longueur des mots pour nettoyer/quitter est identique (le code pour nettoyer est venu après)
- il faut faire attention à ce que verr.maj. ne soit pas enclenché pour nettoyer/quitter (et que le raccourci pour passer du clavier azerty vers le bépo non plus…)
- pas vraiment du plein écran sur certains windows manager (comme awesome-wm). Je n’ai pas trop cherché pourquoi.
- je n’ai pas bloqué les touches multimédia, y compris concernant le son… et plein de sons d’animaux, trop fort, c’est trop fort…

Plus d'infos : [http://fredtantini.free.fr/2014/02/20140205_gribouilli_pour_laisser_vos_petits_enfants_taper_sur_votre_clavier.html]