#! /bin/bash

URL="http://soundbible.com/tags-animal.html"

mkdir -p sounds

#cd sounds
#wget -r -nd -A.mp3 -w 3 $URL
#cd ..

while read line
do
    name=$(echo $line|cut -d'"' -f4|cut -d'=' -f2|cut -d'&' -f1)
    wget -O "sounds/${name##*/}" "$name"
    sleep 3
done < <(grep theFile /tmp/tmp_gribouilli |grep embed)


