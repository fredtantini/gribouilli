#!/usr/bin/python
# -*- coding: utf-8 -*-
"""drawing/smashing keyboard program"""

import sys, random, os, math
from collections import defaultdict
from PyQt4 import QtGui, QtCore
from PyQt4.phonon import Phonon
class Gribouilli(QtGui.QWidget):
    """main class"""
    def __init__(self):
        super(Gribouilli, self).__init__()
        self.initUI()
        
    def initUI(self):
        """initializes variables, loads sounds and images"""
        fullSize = QtGui.QDesktopWidget().screenGeometry().size()
        self.background = QtGui.QImage(fullSize, QtGui.QImage.Format_RGB32)
        self.background.fill(QtGui.qRgb(255, 255, 255))
        self.lastPoint = None
        self.penSize = 1
        self.grow = 1
        self.setMouseTracking(True)
        self.showFullScreen()
        self.word = ""
        self.password = "iwanttoquit!" # "jeveuxquitter!"
        self.clear = "iwanttoclean"    # "jeveuxnettoyer"
        loadedSounds = [os.path.join(os.getcwd(), "sounds", i)
                       for i in os.listdir(os.path.join(os.getcwd(), "sounds"))]
        loadedImages = [os.path.join(os.getcwd(), "images", i)
                       for i in os.listdir(os.path.join(os.getcwd(), "images"))]
        self.sounds = defaultdict(list)
        self.images = defaultdict(list)
        if loadedSounds:
            lSounds = []
            for i in loadedSounds:
                category = os.path.basename(i).split('.')[0]#cat_2 for cat_2.wav
                if '_' in category :
                    last = category.split('_')[-1]
                    if last.isdigit():
                        category = '_'.join(category.split('_')[:-1])
                #category=cat for cat.wav, cat_2.wav, cat_meow for cat_meow.wav…
                lSounds += [ (category, i) ]
            for category, path in lSounds:
                self.sounds[category].append(path)
        if loadedImages:
            lImages = []
            for i in loadedImages:
                category = os.path.basename(i).split('.')[0]
                if '_' in category :
                    last = category.split('_')[-1]
                    if last.isdigit():
                        category = '_'.join(category.split('_')[:-1])
                lImages += [ (category, i) ]
            for category, path in lImages:
                self.images[category].append(path)
        self.chosenCategory = None
        self.m_media = Phonon.MediaObject(self)
        audioOutput = Phonon.AudioOutput(Phonon.NotificationCategory, self)
        Phonon.createPath(self.m_media, audioOutput)
 
    def play(self):
        """
        randomly plays a sound from the dict 'sounds'
        sets which 'category' in 'chosenCategory'
        """
        if self.sounds:
            self.chosenCategory = random.choice(self.sounds.keys())
            snd = random.choice(self.sounds[self.chosenCategory])
            rnd = QtCore.QString(snd)
            if (QtCore.QFile(rnd).exists()):
                self.m_media.setCurrentSource(Phonon.MediaSource(rnd))
                self.m_media.play()
                
             
    def paintEvent(self, _):
        """paintEvent"""
        qp = QtGui.QPainter()
        qp.begin(self)
        qp.drawImage(0, 0, self.background)
        qp.end()
        

    def mouseMoveEvent(self, event):
        """when the mouse move : we have to draw a line"""
        if(not self.lastPoint):
            self.lastPoint = QtCore.QPoint(event.pos())
        self.drawLineTo(event.pos())

    def drawLineTo(self, endPoint):
        """
        draws a line with a random color
        the size of the line grows each time
        """
        painter = QtGui.QPainter()
        painter.begin(self.background)
        r, g, b = [random.randint(0, 255) for _ in range(3)]        
        self.penSize += self.grow
        if self.penSize > 10:
            self.grow = -1
        if self.penSize < 2:
            self.grow = 1
        painter.setPen(QtGui.QPen(QtGui.QColor(r, g, b), self.penSize,
                                  QtCore.Qt.SolidLine, QtCore.Qt.RoundCap,
                                  QtCore.Qt.RoundJoin))
        painter.drawLine(self.lastPoint, endPoint)
        painter.end()
        self.update()
        self.lastPoint = QtCore.QPoint(endPoint)

    def keyPressEvent(self, event):
        """
        checks if we have to
        quit
        clear the board
        otherwise, we draw something
        """
        letter = event.text()
        self.word += letter
        if (len(self.word) > len(self.password)):
            self.word = self.word[1:len(self.word)]
        if (self.word == self.password):
            self.close()
        elif (self.word == self.clear):
            self.clearBoard()
        else:
            self.drawSomething()
        
    def mousePressEvent(self, event):
        """ if we click, we draw something """
        self.drawSomething((event.pos().x(), event.pos().y()))
    
    def clearBoard(self):
        """just fill with blank"""
        self.background.fill(QtGui.qRgb(255, 255, 255))
        self.update()

    def drawSomething(self, pos = None):
        """
        starts by playing something.
        then chose a color, a position, a size.
        if the category of the sound matches a category of images, display it
        otherwise, draw something nice
        """
        self.play()
        #color
        r, g, b = [random.randint(0, 255) for _ in range(3)]
        color = QtGui.QColor(r, g, b)
        #pos
        if (not pos):
            w = self.size().width()
            h = self.size().height()
            x = random.randint(1, w - 1)
            y = random.randint(1, h - 1)
        else:
            x, y = pos
        #sizes
        size1 = random.randint(20, 200)
        size2 = random.randint(20, 200)

        qp = QtGui.QPainter()
        qp.begin(self.background)
        qp.setBrush(QtGui.QBrush(color))
        qp.setPen(QtGui.QPen(color))
        if self.chosenCategory in self.images:
            self.pasteImage(qp, x, y, size1, size2)
        else:
            self.chosenCategory = random.choice(self.images.keys())
            {1: myEllipse,
             2: myRectangle,
             3: myStar,
             4: self.pasteImage
             }[random.randint(1, 3)](qp, x, y, size1, size2)
             #}[3](qp, x, y, size1, size2)            
        qp.end()
        self.update()
                
    def pasteImage(self, qp, x, y, size1, size2):
        """
        print an image from the 'images' dict, depending on the chosenCategory
        chosenCategory has to be in the 'images' dict
        """
        img = QtGui.QImage()
        pic = random.choice(self.images[self.chosenCategory])
        img.load(pic)
        img2 = img.scaled(size1, size2, QtCore.Qt.KeepAspectRatio)
        x, y = x - img2.width() / 2, y - img2.height() / 2
        qp.drawImage(x, y, img2)



def myEllipse(qp, x, y, size1, size2):
    """draws an ellipse"""
    qp.drawEllipse(x - size1 / 2, y - size2 / 2, size1, size2)

def myRectangle(qp, x, y, size1, size2):
    """draws a rectangle"""
    qp.drawRect(x - size1 / 2, y - size2 / 2, size1, size2)

def myStar(qp, x, y, size1, size2):
    """draws a star"""
    listOfPoints = []
    npoints = random.randint(5, 25)
    step = 2#random.randint(1, 4)
    for i in range(npoints):
        nx = x + size1 * math.cos(step * i * 2 * math.pi / npoints)
        ny = y + size2 * math.sin(step * i * 2 * math.pi / npoints)
        listOfPoints.append(QtCore.QPointF(nx, ny))
    star = QtGui.QPolygonF(listOfPoints)
    qp.drawPolygon(star, QtCore.Qt.WindingFill)

        
def main():
    """main"""
    app = QtGui.QApplication(sys.argv)
    QtGui.QApplication.setApplicationName("Gribouilli")
    _ = Gribouilli()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()

    
